# maas-worker

This role aims to deploy a maas worker on a given host.

## Usage

`requirements.yml`:
```yaml
roles:
  - name: cri.maas_worker
    src: git+https://gitlab.cri.epita.fr/cri/iac/ansible/roles/maas_worker.git
    version: 0.1.3
```

Example of playbook:
```yaml
---

- hosts: all
  roles:
    - {role: cri.maas_worker, tags: ['maas']}
```

With its variables file:
```yaml
maas_worker_scheduler_base_url: https://maas.example.org
maas_worker_scheduler_url: wss://maas.example.org/ws/
maas_worker_register_token: <WORKER_REGISTRATION_TOKEN>
maas_version: 0.1.29
maas_proxy_host: <HTTP_PROXY_HOST>
maas_proxy_port: <HTTP_PROXY_PORT>
maas_registries:
  - url: registry.example.org
    token: <REGISTRY_TOKEN>
```
